class User < ApplicationRecord

	require 'csv'
	validates :name, :email, :number, presence: true
													
	def self.import(file)
					CSV.foreach(file.path, { encoding: "UTF-8", headers: true, header_converters: :symbol, converters: :all}) do |row|
			User.create! row.to_hash
		end
	end

end
